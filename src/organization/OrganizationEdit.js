import React from "react";
import { Mutation } from "react-apollo";
import {
  CREATE_ORGANIZATION,
  GET_ORGANIZATIONS,
  UPDATE_ORGANIZATION,
  DELETE_ORGANIZATION
} from "./OrganizationQl";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { setState } from './OrganizationActions'

export class OrganizationEdit extends React.Component {
  defaults = {
    completed: false
  };

  state = {
    ...this.defaults
  };

  setDefaults() {
    this.setState(this.defaults);
  }

  createOrganization(mutation) {
    let { _id, name, email } = this.props.item;
    mutation({ variables: { record: { _id, name, email } } });
  }

  updateOrganization(mutation) {
    let { _id, name, email } = this.props.item;
    mutation({ variables: { record: { _id, name, email } } });
  }

  deleteOrganization(mutation) {
    let { _id } = this.props.item;
    mutation({ variables: { _id}});
  }


  onComplete() {
    this.setDefaults();
    this.props.setState({_id: null, name: '', email: ''});
    this.setState({ completed: true });
  }

  render() {
    let { _id, name, email } = this.props.item;

    if (this.state.completed) {
      return <Redirect to="/" />;
    }

    return (
      <div className="content-section implementention">
        <h3 className="first">Crie uma organização</h3>
        <div className="p-fluid">
          <div className="p-md-4">
            <InputText
              value={name}
              placeholder="Nome da organização"
              onChange={e => this.props.setState({name: e.target.value })}
            />
          </div>
          <div className="p-md-4">
            <InputText
              value={email}
              placeholder="E-mail de faturamento"
              onChange={e => this.props.setState({email: e.target.value })}
            />
          </div>
          <div className="p-grid p-col-4">
            <div className="p-md-4">
              {_id ? (
                <UpdateMutation
                  onCompleted={() => this.onComplete()}
                  onClick={mutation => {
                    this.updateOrganization(mutation);
                  }}
                />
              ) : (
                <CreateMutation
                  className="p-button-success"
                  onCompleted={() => this.onComplete()}
                  onClick={mutation => {
                    this.createOrganization(mutation);
                  }}
                />
              )}
            </div>
            <div className="p-md-4">
                <Button label="Cancelar" 
                  className="p-button-secondary" 
                  onClick={() => this.onComplete()}

                />
            </div>
            {_id ? (
              <div className="p-md-4">
                <DeleteMutation
                  onCompleted={() => this.onComplete()}
                  onClick={mutation => {
                    this.deleteOrganization(mutation);
                  }}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const MutationButton = ({ label, onClick, className, ...props }) => (
  <Mutation {...props}>
    {(mutation, { data }) => (
      <Button
        label={label}
        onClick={() => onClick(mutation, data)}
        className={className}
      />
    )}
  </Mutation>
);

const CreateMutation = props => (
  <MutationButton
    {...props}
    label="Salvar"
    mutation={CREATE_ORGANIZATION}
    update={(
      cache,
      {
        data: {
          createOrganization: { record }
        }
      }
    ) => {
      const { organizations } = cache.readQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 }
      });

      cache.writeQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 },
        data: {
          organizations: organizations.concat([record])
        }
      });
    }}
  />
);

const UpdateMutation = props => (
  <MutationButton
    {...props}
    label="Alterar"
    mutation={UPDATE_ORGANIZATION}
    update={(
      cache,
      {
        data: {
          updateOrganizationById: { record: org }
        }
      }
    ) => {
      const { organizations } = cache.readQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 }
      });

      let index = organizations.findIndex(e => e._id === org._id);
      if (index > -1) {
        organizations[index] = org;
      }

      cache.writeQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 },
        data: {
          organizations: organizations
        }
      });
    }}
  />
);

const DeleteMutation = props => (
  <MutationButton
    {...props}
    className="p-button-danger"
    label="Excluir"
    mutation={DELETE_ORGANIZATION}
    update={(cache, { data: {deleteOrganizationById: {recordId} }}) => {
      const { organizations } = cache.readQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 }
      });

      let index = organizations.findIndex(e => e._id === recordId);
      if (index > -1) {
        organizations.splice(index, 1);
      }

      cache.writeQuery({
        query: GET_ORGANIZATIONS,
        variables: { skip: 0 },
        data: {
          organizations: organizations
        }
      });
    }}
  />
);

const mapStateToProps = store => ({
  item: store.organization
});

const mapDispatchToProps = {
  setState
}


export default connect(mapStateToProps,mapDispatchToProps)(OrganizationEdit);
