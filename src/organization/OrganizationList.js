import React from 'react'
import { Link } from 'react-router-dom'
import { Query} from 'react-apollo'
import { Button } from 'primereact/button'
import { GET_ORGANIZATIONS} from './OrganizationQl'
import { selectItem } from './OrganizationActions'
import { connect } from 'react-redux'

const OrganizationList = ({selectItem}) => (
  <div className='p-fluid'>
    <div className='p-grid'>
      <div className='p-col-4'>
        <h1>Organizacões</h1>
      </div>
      <div className='p-col-2'>
        <h1>
          <Link to='/org/create'>
            <Button className='p-button-secondary' label='Nova organização' />
          </Link>
        </h1>
      </div>
    </div>
    <div>
      <Query query={GET_ORGANIZATIONS} variables={{ skip: 0 }}>
        {({ loading, error, data }) => {
          if (loading) {
            return <div>Loading</div>
          }
          if (error) {
            return <h1>ERROR</h1>
          }

          let { organizations } = data

          return organizations.map(org => (
            <div
              key={org._id}
              className='p-grid p-fluid'
              style={{ width: '50%', border: '1px solid #e1e4e8' }}
            >
              <div className='p-col-4'>
                <Link to='/org/create' onClick={()=>{selectItem(org)}}>{org.name}</Link>
              </div>
              <div className='p-col-4'>
                {org.email}
              </div>
            </div>
          ))
        }}
      </Query>
    </div>
  </div>
)

const mapDispatchToProps = {
  selectItem
}

export default connect(
  null,
  mapDispatchToProps
)(OrganizationList)
