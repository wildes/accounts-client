import gql from "graphql-tag";

export const GET_ORGANIZATIONS = gql`
  query organizations($skip: Int!) {
    organizations(skip: $skip) {
      _id
      name
      email
    }
  }
`;

export const CREATE_ORGANIZATION = gql`
  mutation CreateOrganization($record: CreateOneOrganizationInput!) {
    createOrganization(record: $record) {
      record {
        _id
        name
        email
      }
    }
  }
`;

export const DELETE_ORGANIZATION = gql`
  mutation DeleteOrganization($_id: MongoID!) {
    deleteOrganizationById(_id: $_id) {
      recordId
    }
  }
`;


export const UPDATE_ORGANIZATION = gql`
  mutation UpdateOrganization($record: UpdateByIdOrganizationInput!) {
    updateOrganizationById(record: $record) {
      record {
          _id
          name
          email
      }
    }
  }
`;