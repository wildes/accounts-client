export const SET_STATE = 'SET_STATE'
export const ORGANIZATION_SELECTED = 'ORGANIZATION_SELECTED'

export const selectItem = item => ({
  type: ORGANIZATION_SELECTED,
  value: item
})

export const setState = state => ({
  type: ORGANIZATION_SELECTED,
  value: state
})


