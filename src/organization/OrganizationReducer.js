import { SET_STATE,ORGANIZATION_SELECTED } from './OrganizationActions'

const initialState = {
  name: '',
  email: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_STATE:
      return {
        ...state,
        ...action.value
      }
    case ORGANIZATION_SELECTED:
      return {
        ...state,
        ...action.value
      }
    default:
      return state
  }
}
