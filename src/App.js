import React from 'react'
import 'primereact/resources/themes/nova-light/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'
import { Route } from 'react-router-dom'

import { OrganizationEdit, OrganizationList } from './organization'

export default class App extends React.Component {
  render () {

    return (
      <div>
        <div>
          <Route exact path='/' component={OrganizationList} />
          <Route path='/org/list' component={OrganizationList} />
          <Route path='/org/create' component={OrganizationEdit} />
        </div>
      </div>
    )
  }
}
