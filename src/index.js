import React from 'react'
import { render } from 'react-dom'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'
import { HashRouter as Router, withRouter } from 'react-router-dom'
import { Provider as ReduxProvider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './reducers'
import App from './App'

const store = createStore(reducers)

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
})

const AppWithRouter = withRouter(App)

const WrappedApp = (
  <ReduxProvider store={store}>
    <ApolloProvider client={client}>
      <Router>
        <AppWithRouter />
      </Router>
    </ApolloProvider>
  </ReduxProvider>
)

render(WrappedApp, document.getElementById('root'))
